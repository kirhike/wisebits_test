import os
import pytest
import sys
from selenium import webdriver


# TODO : Fix tests for Firefox
@pytest.fixture(scope="session")
def browser():
    name = "chrome"
    current_dir = os.path.dirname(os.path.realpath(__file__))
    if name == "chrome":
        driver = webdriver.Chrome(
            executable_path=os.path.join(
                current_dir, "drivers", "chromedriver"
            )
        )
    elif name == "firefox":
        driver = webdriver.Firefox(
            executable_path=os.path.join(
                current_dir, "drivers", "firefoxdriver"
            )
        )
    yield driver
    driver.quit()
