from random import randint

from .pages import BasePage
import pytest


@pytest.mark.parametrize("expected_name, expected_address", [("Giovanni Rovelli", "Via Ludovico il Moro 22")])
def test_check_giovanny_address(browser, expected_name, expected_address):
    page = BasePage(browser)
    page.go_to_site()
    page.change_query("Select * from Customers;")
    page.submit_form()
    results = page.get_results()

    giovanni_rows = [r for r in results if r.ContactName == expected_name]
    assert (
            len(giovanni_rows) == 1
    ), f"Expected one row with ContactName = {expected_name}"
    assert giovanni_rows[0].ContactName == expected_name
    assert giovanni_rows[0].Address == expected_address


@pytest.mark.parametrize("expected_city, expected_rows", [("London", 6)])
def test_check_london_city(browser, expected_city, expected_rows):
    page = BasePage(browser)
    page.go_to_site()
    page.change_query(f'SELECT * FROM Customers where City = "{expected_city}";')
    page.submit_form()
    results = page.get_results()
    london_rows = [r.City for r in results if r.City == expected_city]

    assert len(results) == expected_rows
    assert len(london_rows) == expected_rows


@pytest.mark.parametrize("expected_id", [randint(91, 999)])
def test_add_new_row_and_check(browser, expected_id):
    page = BasePage(browser)
    page.go_to_site()
    page.change_query('insert into Customers('
                      '"CustomerID", "CustomerName", "ContactName", "Address", "City", '
                      '"PostalCode", "Country") '
                      'values('
                      f'"{expected_id}", "Test customer", "Test Name", "Test Address", '
                      '"Test City", "12345", "Test Country");'
                      )
    page.submit_form()
    page.change_query(f'SELECT * FROM Customers where CustomerID = "{expected_id}";')
    page.submit_form()
    results = page.get_results()
    assert int(results[0].CustomerID) == expected_id


@pytest.mark.parametrize("expected_id, expected_city", [(randint(1, 90), "Moscow")])
def test_update_new_row_and_check(browser, expected_id, expected_city):
    page = BasePage(browser)
    page.go_to_site()
    page.change_query(f'Update Customers set City = "{expected_city}" Where CustomerID = {expected_id};')
    page.submit_form()
    page.change_query(f'SELECT * FROM Customers where CustomerID = "{expected_id}";')
    page.submit_form()
    results = page.get_results()
    assert results[0].City == expected_city
    page.reset_form()

#TODO: add fixture to reset data after tests