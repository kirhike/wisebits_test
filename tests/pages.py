from time import sleep
from typing import List
from collections import namedtuple

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class BasePage:
    submit_button_txt = "Run SQL"
    reset_button_txt = "Restore Database"
    base_url = (
        "https://www.w3schools.com/sql/trysql.asp?filename=trysql_select_all"
    )

    def __init__(self, driver, time: int = 10):
        self.driver = driver
        self.wait = WebDriverWait(driver, time)

    def go_to_site(self):
        return self.driver.get(self.base_url)

    def click_button(self, name: str) -> None:
        if name == self.submit_button_txt:
            element = self.wait.until(
                EC.element_to_be_clickable(
                    (By.XPATH, "/html/body/div[2]/div/div[1]/div[1]/button")
                )
            )
            element.click()
        if name == self.reset_button_txt:
            element = self.wait.until(
                EC.element_to_be_clickable(
                    (By.ID, "restoreDBBtn")
                )
            )
            element.click()

    def submit_form(self) -> None:
        self.click_button(self.submit_button_txt)
        sleep(1)

    def reset_form(self) -> None:
        self.click_button(self.reset_button_txt)

    def change_query(self, query: str) -> None:
        self.driver.execute_script(
            f"window.editor.getDoc().setValue('{query}')"
        )


    def get_results(self) -> List[namedtuple]:
        result = []
        table_id = self.wait.until(
            EC.presence_of_element_located(
                (By.XPATH, "//*[@id='divResultSQL']/div/table")
            )
        )
        rows = table_id.find_elements(By.TAG_NAME, "tr")
        header = rows[0]
        body = rows[1:]

        header_columns = header.find_elements_by_tag_name("th")

        column_names = [
            column.text.strip().replace(" ", "") for column in header_columns
        ]

        Item = namedtuple("Item", column_names)

        for i, row in enumerate(body, start=1):
            columns = [
                column.text for column in row.find_elements_by_tag_name("td")
            ]
            result.append(Item(*columns))
        return result




